<?php
session_start();
$myFile = "assets/" . $_GET['quotation'] . ".json";
$arr_data = array(); // create empty array
$jsondata = file_get_contents($myFile);
$arr_data = json_decode($jsondata, true);
$currencySymbol = "&#36;";
if ($arr_data['pricing_info']['currency'] === "usd") {
    $currencySymbol = "&#36;";
} else {
    $currencySymbol = "&#163;";
}
?>
<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <title>ContractPodAi - Price Calculator</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link rel="icon" href="favicon.ico">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="MDB/css/bootstrap.min.css">
    <link rel="stylesheet" href="MDB/css/mdb.min.css">
    <link rel="stylesheet" href="MDB/css/style.css">
    <link rel="stylesheet" href="css/iao-alert.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/quotation.css">
    <meta name="theme-color" content="#fafafa">
    <script src="js/modernizr-3.8.0.min.js"></script>
    <script type="text/javascript" src="MDB/js/jquery.min.js"></script>
    <script type="text/javascript" src="MDB/js/popper.min.js"></script>
    <script type="text/javascript" src="MDB/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="MDB/js/mdb.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="build/pdfmake.min.js"></script>
    <script src="build/vfs_fonts.js"></script>
    <script src="js/iao-alert.jquery.min.js"></script>
</head>

<body>
<!--[if IE]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->

<!-- Add your site or application content here -->
<div class="view_bg">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="title-container">
                    <h3><?php echo $_SESSION["name"]; ?></h3>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <div class="col-md-6">
                        <div class="title-container">
                            <a href="dataInputCopy.php">Re-calculate</a>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="title-container">
                            <a href="index.php">Logout</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        if ($arr_data['license_info']['totalUsers'] > 1000 || $arr_data['license_info']['fullUser'] > 500) {
            ?>
            <div class="row pt-5">
                <div class="col text-left" id="pageHeader">
                    <h2>This customer needs <span class="coloredText">a custom quote due to the volume of sale,</span>  please speak to your VP.</h2>
                </div>
            </div>
            <?php
        }
        else {
            ?>
            <div class="row d-flex justify-content-center">
                <div class="col-md-10 col-xl-6 py-5">
                    <div class="card">
                        <div class="card-header card-image d-flex justify-content-center">
                            <h3>Pricing Calculation Result</h3>
                        </div>
                        <div class="card-body px-lg-5">
                            <form class="text-center" style="color: #757575;" id="calculateResultForm">
                                <div class="md-form mt-3">
                                    <label for="quotationNumber" class="active">* Quotation #</label>
                                    <input type="text" class="form-control is-valid" id="quotationNumber"
                                           name="quotationNumber" autocomplete="off"
                                           value="<?php echo $arr_data['quotation_info']['quotation_number']; ?>">
                                </div>
                                <div class="md-form mt-3">
                                    <input type="text" id="quotationDate" name="quotationDate"
                                           class="form-control datepicker" required
                                           value="<?php echo $arr_data['quotation_info']['quotation_date']; ?>">
                                    <label for="quotationDate" class="active">* Quotation Date</label>
                                </div>
<!--                                <div class="md-form mt-3">-->
<!--                                    <input type="text" id="expiryDate" name="expiryDate" class="form-control datepicker"-->
<!--                                           autocomplete="off"-->
<!--                                           value="--><?php //echo $arr_data['quotation_info']['expiry_date']; ?><!--">-->
<!--                                    <label for="expiryDate" class="active">Expiry Date</label>-->
<!--                                </div>-->
                                <div class="md-form mt-3">
                                    <div class="form-check form-check-inline">
                                        <label for="fullUsers" class="active">Full users license</label>
                                        <input type="number" class="form-control is-valid" id="fullUsers"
                                               name="fullUsers" autocomplete="off"
                                               value="<?php echo $arr_data['license_info']['fullUser']; ?>">
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <label for="businessUsers" class="active">Business users license</label>
                                        <input type="number" class="form-control is-valid" id="businessUsers"
                                               name="businessUsers" autocomplete="off"
                                               value="<?php echo $arr_data['license_info']['businessUsers']; ?>">
                                    </div>
                                </div>
<!--                                <div class="md-form mt-3">-->
<!--                                    <div class="form-check form-check-inline">-->
<!--                                        <label for="fullUsersPrice" class="active">Price for full user license</label>-->
<!--                                        <input type="text" class="form-control is-valid" id="fullUsersPrice"-->
<!--                                               name="fullUsersPrice" autocomplete="off"-->
<!--                                               value="--><?php //echo $currencySymbol . " " . $arr_data['pricing_info']['full_user_price']; ?><!--">-->
<!--                                    </div>-->
<!--                                    <div class="form-check form-check-inline">-->
<!--                                        <label for="businessUsersPrice" class="active">Price for business user-->
<!--                                            license</label>-->
<!--                                        <input type="text" class="form-control is-valid" id="businessUsersPrice"-->
<!--                                               name="businessUsersPrice" autocomplete="off"-->
<!--                                               value="--><?php //echo $currencySymbol . " " . $arr_data['pricing_info']['business_user_price']; ?><!--">-->
<!--                                    </div>-->
<!--                                </div>-->
                                <div class="md-form mt-3">
                                    <label for="premiumUsers" class="active">Contract Risk & Compliance feature</label>
                                    <input type="text" class="form-control is-valid" id="premiumUsers"
                                           name="premiumUsers" autocomplete="off"
                                           value="<?php echo($arr_data['license_info']['premiumUser'] ? $currencySymbol . " " . $arr_data['pricing_info']['premiumPrice'] : $currencySymbol . " 0.00") ?>">
                                </div>
                                <div class="md-form mt-3">
                                    <div class="form-check form-check-inline">
                                        <label for="totalUsers" class="active">Total number of licenses</label>
                                        <input type="number" class="form-control is-valid" id="totalUsers"
                                               name="totalUsers" autocomplete="off"
                                               value="<?php echo $arr_data['license_info']['totalUsers']; ?>">
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <label for="totalUsersPrice" class="active">Total price of licenses</label>
                                        <input type="text" class="form-control is-valid" id="totalUsersPrice"
                                               name="totalUsersPrice" autocomplete="off"
                                               value="<?php echo $currencySymbol . " " . $arr_data['pricing_info']['total_price']; ?>">
                                    </div>
                                </div>
                                <!--                            <div class="md-form mt-3">-->
                                <!--                                <label for="attentionTo">* Attention</label>-->
                                <!--                                <input type="text" class="form-control is-valid" id="attentionTo" name="attentionTo" autocomplete="off" required>-->
                                <!--                            </div>-->
                                <!--                            <div class="md-form mt-3">-->
                                <!--                                <label for="addressLine1">* Street address, P.O. box, company name, c/o</label>-->
                                <!--                                <input type="text" class="form-control is-valid" id="addressLine1" name="addressLine1" autocomplete="off" required>-->
                                <!--                            </div>-->
                                <!--                            <div class="md-form mt-3">-->
                                <!--                                <label for="addressLine2">Apartment, suite , unit, building, floor, etc.</label>-->
                                <!--                                <input type="text" class="form-control is-valid" id="addressLine2" name="addressLine2" autocomplete="off">-->
                                <!--                            </div>-->
                                <!--                            <div class="md-form mt-3">-->
                                <!--                                <div class="form-check form-check-inline">-->
                                <!--                                    <label for="cityName">* City / Town</label>-->
                                <!--                                    <input type="text" class="form-control is-valid" id="cityName" name="cityName" autocomplete="off" required>-->
                                <!--                                </div>-->
                                <!--                                <div class="form-check form-check-inline">-->
                                <!--                                    <label for="stateName">* State / Province</label>-->
                                <!--                                    <input type="text" class="form-control is-valid" id="stateName" name="stateName" autocomplete="off" required>-->
                                <!--                                </div>-->
                                <!--                            </div>-->
                                <!--                            <div class="md-form mt-3">-->
                                <!--                                <div class="form-check form-check-inline">-->
                                <!--                                    <label for="postalCode">* Zip / Postal Code</label>-->
                                <!--                                    <input type="text" class="form-control is-valid" id="postalCode" name="postalCode" autocomplete="off" required>-->
                                <!--                                </div>-->
                                <!--                                <div class="form-check form-check-inline">-->
                                <!--                                    <label for="countryName">* Country</label>-->
                                <!--                                    <input type="text" class="form-control is-valid" id="countryName" name="countryName" autocomplete="off" required>-->
                                <!--                                </div>-->
                                <!--                            </div>-->
                                <!--                            <button class="btn btn-custom-orange btn-rounded btn-block z-depth-0 my-4 waves-effect" type="submit" id="btnCalculate">Generate Quotation</button>-->
                            </form>
                            <!-- Form -->
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>
<script src="js/scripts.js"></script>
<script>
    $('#calculateResultForm').submit (function (e) {
        e.preventDefault();
        $.ajax({
            url: 'generateQuotation.php',
            dataType: 'json',
            type: 'post',
            contentType: 'application/x-www-form-urlencoded',
            data: $('#calculateResultForm').serialize(),
            success: function( data, textStatus, jQxhr ){
                if (data) {
                    window.location = "quotationCopy.php?quotation=" + $('#quotationNumber').val();
                }
                else {
                    $.iaoAlert({
                        msg: "Some error occurred while calculating final price. Please contact admin.",
                        type: "error",
                        mode: "dark",
                        autoHide: true,
                        alertTime: "6000",
                        position: 'top-right',
                        fadeOnHover: false,
                        zIndex: '999'
                    });
                }
            },
            error: function( jqXhr, textStatus, errorThrown ){
                $.iaoAlert({
                    msg: "Some error occurred while calculating final price. Please contact admin.",
                    type: "error",
                    mode: "dark",
                    autoHide: true,
                    alertTime: "6000",
                    position: 'top-right',
                    fadeOnHover: false,
                    zIndex: '999'
                });
            }
        });
    });
</script>
</body>
</html>
