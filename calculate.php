<?php
session_start();
try
{
    $baseCurrencyValue = [2200, 1679];
    $priceGroupFullUserUSD = [1900, 1200];
    $priceGroupBusinessUserUSD = [900, 600];
    $priceGroupFullUserGBP = [1450, 687];
    $priceGroupBusinessUserGBP = [916, 458];
    $nTotalUser = 0;
    $nFullUser = 0;
    $nBusinessUser = 0;
    $totalPrice = 0;
    $fullUserPrice = 0;
    $businessUserPrice = 0;
    $currencyType = $_POST['radioCurrency'];
    if (isset($_POST['totalUsers']) && !empty($_POST['totalUsers'])) {
        $nTotalUser = $_POST['totalUsers'];
    }
    if (isset($_POST['fullUsers']) && !empty($_POST['fullUsers'])) {
        $nFullUser = $_POST['fullUsers'];
    }
    if (isset($_POST['businessUsers']) && !empty($_POST['businessUsers'])) {
        $nBusinessUser = $_POST['businessUsers'];
    }
    if ($currencyType === "usd") {
        $totalPrice = 25 * $baseCurrencyValue[0];
        if ($nTotalUser > 25) {
            for ($i=26; $i<=$nTotalUser; $i++) {
                if ($i < 101) {
                    if ($i <= $nFullUser) {
                        $fullUserPrice = $fullUserPrice + $priceGroupFullUserUSD[0];
                    }
                    else {
                        $businessUserPrice = $businessUserPrice + $priceGroupBusinessUserUSD[0];
                    }
                } else if ($i < 501) {
                    if ($i <= $nFullUser) {
                        $fullUserPrice = $fullUserPrice + $priceGroupFullUserUSD[1];
                    }
                    else {
                        $businessUserPrice = $businessUserPrice + $priceGroupBusinessUserUSD[1];
                    }
                }
            }
        }
    }
    else {
        $totalPrice = 25 * $baseCurrencyValue[1];
        if ($nTotalUser > 25) {
            for ($i=26; $i<=$nTotalUser; $i++) {
                if ($i < 101) {
                    if ($i <= $nFullUser) {
                        $fullUserPrice = $fullUserPrice + $priceGroupFullUserGBP[0];
                    }
                    else {
                        $businessUserPrice = $businessUserPrice + $priceGroupBusinessUserGBP[0];
                    }
                } else if ($i < 501) {
                    if ($i <= $nFullUser) {
                        $fullUserPrice = $fullUserPrice + $priceGroupFullUserGBP[1];
                    }
                    else {
                        $businessUserPrice = $businessUserPrice + $priceGroupBusinessUserGBP[1];
                    }
                }
            }
        }
    }
    $totalPrice = number_format($totalPrice + $fullUserPrice + $businessUserPrice);
    $myFile = "assets/pricing.json";
    $arr_data = array(); // create empty array
    try
    {
        //Get form data
        $formdata = array(
            'quotation_info' => array(
                'quotation_number' => $_POST['quotationNumber'],
                'quotation_date' => $_POST['quotationDate'],
                'expiry_date' => $_POST['expiryDate']
            ),
            'customer_info' => array(
                'attention_to' => $_POST['attentionTo'],
                'address_line1' => $_POST['addressLine1'],
                'address_line2' => $_POST['addressLine2'],
                'city_name' => $_POST['cityName'],
                'state_name' => $_POST['stateName'],
                'postal_code' => $_POST['postalCode'],
                'country_name' => $_POST['countryName']
            ),
            'license_info' => array(
                'totalUsers' => $_POST['totalUsers'],
                'fullUser' => $_POST['fullUsers'],
                'businessUsers' => $_POST['businessUsers']
            ),
            'pricing_info' => array(
                'total_price' => $totalPrice,
                'full_user_price' => $fullUserPrice,
                'business_user_price' => $businessUserPrice,
                'currency' => $_POST['radioCurrency']
            )
        );
        $fp = fopen('assets/' . $_POST['quotationNumber'] . '.json', 'w');
        fwrite($fp, json_encode($formdata));
        fclose($fp);
        echo json_encode(['message' => 'Data saved successfully']);
    }
    catch (Exception $e) {
        echo 'Caught exception: ',  $e->getMessage(), "\n";
    }
}
catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}
