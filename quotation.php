<?php
    $myFile = "assets/" . $_GET['quotation'] . ".json";
    $arr_data = array(); // create empty array
    $jsondata = file_get_contents($myFile);
    $arr_data = json_decode($jsondata, true);
    $currencySymbol = "&#36;";
    if ($arr_data['pricing_info']['currency'] === "usd") {
        $currencySymbol = "&#36;";
    } else {
        $currencySymbol = "&#163;";
    }
?>
<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <title>ContractPodAi - Price Calculator</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link rel="icon" href="favicon.ico">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="MDB/css/bootstrap.min.css">
    <link rel="stylesheet" href="MDB/css/mdb.min.css">
    <link rel="stylesheet" href="MDB/css/style.css">
    <link rel="stylesheet" href="css/iao-alert.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/quotation.css">
    <meta name="theme-color" content="#fafafa">
    <script src="js/modernizr-3.8.0.min.js"></script>
    <script type="text/javascript" src="MDB/js/jquery.min.js"></script>
    <script type="text/javascript" src="MDB/js/popper.min.js"></script>
    <script type="text/javascript" src="MDB/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="MDB/js/mdb.min.js"></script>
    <script src="js/iao-alert.jquery.min.js"></script>
</head>

<body>
<!--[if IE]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->

<!-- Add your site or application content here -->
<div class="view_bg" style="background-image: url('img/background.png'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
    <div class="container">
        <div class="row">
            <div class="col-md-11"></div>
            <div class="col-md-1">
                <div class="logout-container">
                    <a href="index.php">Logout</a>
                </div>
            </div>
        </div>
        <div id="invoice">
            <div class="toolbar hidden-print">
                <div class="text-right">
                    <button class="btn btn-custom-orange"><i class="fa fa-file-pdf-o"></i> Export as PDF</button>
                </div>
                <hr>
            </div>
            <div class="invoice overflow-auto">
                <div style="min-width: 600px">
                    <header>
                        <div class="row">
                            <div class="col" id="logo_padding">
                                <a target="_blank" href="http://contractpodai.com">
                                    <img src="img/logo/logo-black-x1.png" data-holder-rendered="true" />
                                </a>
                            </div>
                            <div class="col company-details">
                                <h3 class="name">
                                    <a target="_blank" href="http://contractpodai.com">
                                        ContractPod Ai
                                    </a>
                                </h3>
                                <div><small>Savoy Tower, Floor 9, 77 Renfrew Street,</small></div>
                                <div><small>GLASGOW, G2 3BZ, Scotland</small></div>
                                <div><small>Tel: +44(0).141.280.1600</small></div>
                            </div>
                        </div>
                    </header>
                    <main>
                        <div class="row contacts">
                            <div class="col invoice-to">
                                <h4>Customer Information</h4>
                                <div class="address">Attn: <b><?php echo $arr_data['customer_info']['attention_to']; ?></b></div>
                                <div class="address"><?php echo $arr_data['customer_info']['address_line1']; ?>, <?php echo $arr_data['customer_info']['address_line2']; ?></div>
                                <div class="address"><?php echo $arr_data['customer_info']['city_name']; ?>, <?php echo $arr_data['customer_info']['state_name']; ?></div>
                                <div class="address"><?php echo $arr_data['customer_info']['postal_code']; ?>, <?php echo $arr_data['customer_info']['country_name']; ?></div>
                            </div>
                            <div class="col invoice-details">
                                <h4 class="invoice-id"><?php echo $arr_data['quotation_info']['quotation_number']; ?></h4>
                                <div class="date">Date of Quotation: <?php echo $arr_data['quotation_info']['quotation_date']; ?></div>
                                <div class="date">Expiry Date: <?php echo $arr_data['quotation_info']['expiry_date']; ?></div>
                            </div>
                        </div>
                        <p>Total License count: <strong><?php echo $arr_data['license_info']['totalUsers']; ?></strong></p>
                        <table class="quote-table" border="0" cellspacing="0" cellpadding="0">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th class="text-left">DESCRIPTION</th>
                                <th class="text-right">LICENSE COUNT</th>
                                <th class="text-right">TOTAL</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="no">01</td>
                                <td class="text-left">Full user licenses</td>
                                <td class="unit"><?php echo $arr_data['license_info']['fullUser']; ?></td>
                                <td class="total"><?php echo $currencySymbol; ?> <?php echo $arr_data['pricing_info']['full_user_price']; ?></td>
                            </tr>
                            <tr>
                                <td class="no">02</td>
                                <td class="text-left">Business user licenses</td>
                                <td class="unit"><?php echo $arr_data['license_info']['businessUsers']; ?></td>
                                <td class="total"><?php echo $currencySymbol; ?> <?php echo $arr_data['pricing_info']['business_user_price']; ?></td>
                            </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="3">SUBTOTAL</td>
                                <td><?php echo $currencySymbol; ?> <?php echo $arr_data['pricing_info']['total_price']; ?></td>
                            </tr>
                            <tr>
                                <td colspan="3">TAX 0%</td>
                                <td><?php echo $currencySymbol; ?> 0.00</td>
                            </tr>
                            <tr>
                                <td colspan="3">GRAND TOTAL</td>
                                <td><?php echo $currencySymbol; ?> <?php echo $arr_data['pricing_info']['total_price']; ?></td>
                            </tr>
                            </tfoot>
                        </table>
                        <div class="notices">
                            <div>NOTICE:</div>
                            <div class="notice">Applicable tax will be recalculated at the time of order processing.<br><br>
                                <small>
                                    In accordance with our terms of business and agreement with you, payment should be made in full <b>14</b> days from date of signature in favour of <b>ContractPod Technologies Limited</b><br>
                                    Please credit the above amount to our bank account details as follows:<br><br>
                                    <table>
                                        <tr><td><b>Account Name:</b> Western Union Business Solutions</td></tr>
                                        <tr><td><b>Bank Name:</b> The Bank of New York Mellon</td></tr>
                                        <tr><td><b>Bank Address:</b> 1 Wall Street, New York City, NY 10286, New York</td></tr>
                                        <tr><td><b>Account No:</b> 8900534389</td></tr>
                                        <tr><td><b>ABA Number:</b> 021000018</td></tr>
                                        <tr><td><b>Swift Code:</b> IRVTUS3N</td></tr>
                                        <tr><td><b>Reference:</b> ContractPod Technologies Limited, NE1600 (please quote this reference number when submitting payment)</td></tr>
                                    </table>
                                    <br>
                                    <p><b>Please email your remittance advice to:</b> <u>accounts@contractpodai.com</u></p>
                                </small>
                            </div>
                        </div>
                    </main>
                    <footer>
                        Invoice was created on a computer and is valid without the signature and seal.
                    </footer>
                </div>
                <!--DO NOT DELETE THIS div. IT is responsible for showing footer always at the bottom-->
                <div></div>
            </div>
        </div>
    </div>
</div>
</body>

</html>
