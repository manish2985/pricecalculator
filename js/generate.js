function generateReport () {
  var docDefinition = {
    info: {
      title: 'ContractPodAi - Quotation',
      author: 'ContractPod',
      subject: 'Quotation'
    },
    pageSize: 'A4',
    pageMargins : [80, 115, 80, 50],
    header: function(currentPage) {
      if (currentPage != 1) {
        return [
          {
            columns: [
              {
                image: 'headerImage',
                width: 595.28,
                height: 79
              }
            ]
          }
        ]
      }
    },
    footer: function(currentPage) {
      if (currentPage != 1) {
        return [
          {
            columns: [
              {text: '© ContractPod Technologies Limited', style: 'footerTextBold'},
            ],
          },
          {
            columns: [
              { text: 'Savoy Tower, Floor 9, 77 Renfrew Street, Glasgow, G2 3BZ, Scotland', style: 'footerText' },
            ]
          },
          {
            columns: [
              { text: 'Tel: +44(0).141.280.1600', style: 'footerText' },
            ]
          },
          {
            columns: [
              { text: 'ContractPod Technologies Limited is a company registered in Scotland: registered number 425646', style: 'footerText' },
            ]
          },
          {
            columns: [
              { text: 'VAT number: 150960419', style: 'footerTextBold' },
            ]
          }
        ]
      }
    },
    content: [],
    styles: {
      header: {
        fontSize: 24,
        bold: true,
        margin: [0, 0, 0, 27]
      },
      invoiceTitle: {
        fontSize: 14,
        bold: true,
        alignment:'right',
        margin:[0,0,0,15],
        color: '#eb7514'
      },
      invoiceSubTitle: {
        fontSize: 10,
        alignment:'right'
      },
      invoiceSubValue: {
        fontSize: 10,
        alignment:'right'
      },
      customerTitle: {
        fontSize: 14,
        bold: true,
        alignment:'left',
        margin:[0,0,0,15],
        color: '#eb7514'
      },
      customerSubTitle: {
        fontSize: 10,
        alignment:'left'
      },
      customerSubTitleBold: {
        fontSize: 10,
        bold: true,
        alignment:'left'
      },
      customerSubValue: {
        fontSize: 10,
        alignment:'left'
      },
      subheader: {
        fontSize: 14,
        bold: true,
        margin: [10, 36, 0, 8]
      },
      boldText: {
        margin: [0, 10, 0, 10],
        fontSize: 10,
        bold: true
      },
      regular: {
        fontSize: 10
      },
      tableBoldText: {
        margin: [0, 10, 0, 10],
        fontSize: 10,
        bold: true
      },
      tableRegularText: {
        margin: [0, 10, 0, 10],
        fontSize: 10
      },
      tableBoldTextColored: {
        margin: [0, 10, 0, 10],
        fontSize: 10,
        bold: true,
        color: '#eb7514'
      },
      taxNoteStyle: {
        fontSize: 8
      },
      noteHeading: {
        fontSize: 8,
        bold: true,
        decoration: 'underline'
      },
      regularNoteText: {
        fontSize: 8
      },
      regularNoteBoldText: {
        fontSize: 8,
        bold: true
      },
      footerTextBold: {
        fontSize: 6,
        alignment:'center',
        bold: true
      },
      footerText: {
        fontSize: 6,
        alignment:'center'
      }
    },
    images: {}
  };
  pdfMake.fonts = {
    Roboto: {
      normal: 'Poppins-Regular.ttf',
      bold: 'Poppins-Medium.ttf',
      italics: 'Poppins-Italic.ttf',
      bolditalics: 'Poppins-MediumItalic.ttf'
    }
  };
  docDefinition.content.push(objectCover);
  docDefinition.content.push(quotationDetails);
  docDefinition.content.push('\n\n\n');
  docDefinition.content.push(quotationTable);
  docDefinition.content.push(quotationSubTotal);
  docDefinition.content.push('\n\n\n');
  docDefinition.content.push(taxNote);
  docDefinition.content.push('\n\n\n');
  docDefinition.content.push(quotationNote);
  docDefinition.images.coverImage = coverImgLight;
  docDefinition.images.headerImage = headerImage;
  pdfMake.createPdf(docDefinition).open();
}


