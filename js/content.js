var attention = $('#attn').val();
var addressLine1 = $('#addressLine1').val();
var addressLine2 = $('#addressLine2').val();
var cityName = $('#cityName').val();
var stateName = $('#stateName').val();
var postalCode = $('#postalCode').val();
var countryName = $('#countryName').val();
var quotationNumber = $('#quotationNumber').val();
var quotationDate = $('#quotationDate').val();
var expiryDate = $('#expiryDate').val();
var totalUser = $('#totalUser').val();
var totalPrice = $('#totalPrice').val();
var currencySymbol = $('#currencySymbol').val();

var objectCover = {
  image: 'coverImage',
  alignment: 'center',
  width: 595.28,
  height: 841.89,
  margin: [0, -115, 0, 0],
  pageBreak: 'after'
};

var quotationDetails = [
  {
    columns: [
        [
          {
            text: 'Customer Information',
            style: 'customerTitle'
          },
          {
            columns: [
              {
                text: [
                  'Attn: ',
                  {
                    text: attention,
                    style: 'customerSubTitleBold'
                  }
                ],
                style:'customerSubTitle'
              }
            ]
          },
          {
            columns: [
              {
                text: addressLine1,
                style:'customerSubTitle'
              }
            ]
          },
          {
            columns: [
              {
                text: addressLine2,
                style:'customerSubTitle'
              }
            ]
          },
          {
            columns: [
              {
                text: cityName + ', ' + stateName,
                style:'customerSubTitle'
              }
            ]
          },
          {
            columns: [
              {
                text: postalCode + ', ' + countryName,
                style:'customerSubTitle'
              }
            ]
          }
        ],
        [
          {
            text: 'Quotation Information',
            style: 'invoiceTitle',
            width: '*'
          },
          {
            columns: [
              {
                text:'Quotation #',
                style:'invoiceSubTitle',
                width: '*'

              },
              {
                text: quotationNumber,
                style:'invoiceSubValue',
                width: 100

              }
            ]
          },
          {
            columns: [
              {
                text:'Quotation Date',
                style:'invoiceSubTitle',
                width: '*'
              },
              {
                text: quotationDate,
                style:'invoiceSubValue',
                width: 100
              }
            ]
          },
          {
            columns: [
              {
                text:'Expiry Date',
                style:'invoiceSubTitle',
                width: '*'
              },
              {
                text: expiryDate,
                style:'invoiceSubValue',
                width: 100
              }
            ]
          }
        ]
    ]
  }
];

var quotationTable = [
  {
    table: {
      widths: [10, 260, 80, 50],
      body: [
        [{border: [false, false, false, true], fillColor: '#eeeeee', text: '#', style: 'boldText'}, {border: [false, false, false, true], fillColor: '#eeeeee', text: 'DESCRIPTION', style: 'boldText'}, {border: [false, false, false, true], fillColor: '#eeeeee', text: 'License Count', style: 'boldText', alignment: 'right'}, {border: [false, false, false, true], fillColor: '#eeeeee', text: 'Total', style: 'boldText', alignment: 'right'}],
        [{border: [false, false, false, false], fillColor: '#eeeeee', text: '1', style: 'boldText', alignment: 'right'}, {border: [false, false, false, false], fillColor: '#eeeeee', text: 'Total user license', style: 'boldText'}, {border: [false, false, false, false], fillColor: '#eeeeee', text: totalUser, style: 'boldText', alignment: 'right'}, {border: [false, false, false, false], fillColor: '#eeeeee', text: currencySymbol + ' ' + totalPrice, style: 'boldText', alignment: 'right'}]
      ]
    }
  }
];
var quotationSubTotal = [
  {
    table: {
      widths: [10, 260, 80, 50],
      body: [
        ['', '', {border: [false, false, false, false], text: 'Subtotal', style: 'tableBoldText'}, {border: [false, false, false, true], text: currencySymbol + ' ' + totalPrice, style: 'tableRegularText', alignment: 'right'}],
        ['', '', {border: [false, false, false, false], text: 'Tax 0%', style: 'tableBoldText'}, {border: [false, false, false, true], text: '$ 0.00', style: 'tableRegularText', alignment: 'right'}],
        ['', '', {border: [false, false, false, false], text: 'Grand Total', style: 'tableBoldTextColored'}, {border: [false, false, false, false], text: currencySymbol + ' ' + totalPrice, style: 'tableBoldTextColored', alignment: 'right'}]
      ]
    },
    layout: {
      defaultBorder: false,
    }
  }
];

var taxNote = [
  {
    text: '** Applicable tax will be recalculated at the time of order processing. **',
    style: 'taxNoteStyle',
    alignment: 'center'
  }
];

var quotationNote = [
  {
    columns: [
      {
        text: 'Note:',
        style: 'noteHeading'
      }
    ]
  },
  {
    style: 'regularNoteText',
    text: [
      'In accordance with our terms of business and agreement with you, payment should be made in full ',
      {
        text: '14',
        style: 'regularNoteBoldText'
      },
      ' days from date of signature in favour of ',
      {
        text: 'ContractPod Technologies Limited',
        style: 'regularNoteBoldText'
      },
    ]
  },
  {
    columns: [
      {
        text: 'Please credit the above amount to our bank account details as follows:',
        style: 'regularNoteText'
      }
    ]
  },
    '\n',
  {
    style: 'regularNoteText',
    text: [
      {
        text: 'Account Name: ',
        style: 'regularNoteBoldText'
      },
      'Western Union Business Solutions'
    ]
  },
  {
    style: 'regularNoteText',
    text: [
      {
        text: 'Bank Name: ',
        style: 'regularNoteBoldText'
      },
      'The Bank of New York Mellon'
    ]
  },
  {
    style: 'regularNoteText',
    text: [
      {
        text: 'Bank Address: ',
        style: 'regularNoteBoldText'
      },
      '1 Wall Street, New York City, NY 10286, New York'
    ]
  },
  {
    style: 'regularNoteText',
    text: [
      {
        text: 'Account No: ',
        style: 'regularNoteBoldText'
      },
      '8900534389'
    ]
  },
  {
    style: 'regularNoteText',
    text: [
      {
        text: 'ABA Number: ',
        style: 'regularNoteBoldText'
      },
      '021000018'
    ]
  },
  {
    style: 'regularNoteText',
    text: [
      {
        text: 'Swift Code: ',
        style: 'regularNoteBoldText'
      },
      'IRVTUS3N'
    ]
  },
  {
    style: 'regularNoteText',
    text: [
      {
        text: 'Reference: ',
        style: 'regularNoteBoldText'
      },
      'ContractPod Technologies Limited, NE1600 (please quote this reference number when submitting payment)\n'
    ]
  },
    '\n',
  {
    style: 'regularNoteText',
    text: [
      {
        text: 'Please email your remittance advice to:',
        style: 'regularNoteBoldText'
      },
      'accounts@contractpodai.com'
    ]
  }
];
