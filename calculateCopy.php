<?php
session_start();
try
{
    $priceGroupFullUserUSD = [1900, 1200];
    $priceGroupBusinessUserUSD = [900, 600, 400];
    $nTotalUser = 0;
    $nFullUser = 0;
    $nBusinessUser = 0;
    $totalPrice = 0;
    $fullUserPrice = 0;
    $businessUserPrice = 0;
    $premiumUser = false;
    $premiumValue = 60000;
    $currencyType = $_POST['radioCurrency'];
    if (isset($_POST['totalUsers']) && !empty($_POST['totalUsers'])) {
        $nTotalUser = $_POST['totalUsers'];
    }
    if (isset($_POST['fullUsers']) && !empty($_POST['fullUsers'])) {
        $nFullUser = $_POST['fullUsers'];
    }
    if (isset($_POST['businessUsers']) && !empty($_POST['businessUsers'])) {
        $nBusinessUser = $_POST['businessUsers'];
    }
    $totalPrice = 25 * 2200;
    $tempTotalPrice = $totalPrice;
    if ($nTotalUser > 25) {
        for ($i=26; $i<=$nTotalUser; $i++) {
            if ($i < 101) {
                if ($i <= $nFullUser) {
                    $fullUserPrice = $fullUserPrice + $priceGroupFullUserUSD[0];
                }
                else {
                    $businessUserPrice = $businessUserPrice + $priceGroupBusinessUserUSD[0];
                }
            } else if ($i <= 500) {
                if ($i <= $nFullUser) {
                    $fullUserPrice = $fullUserPrice + $priceGroupFullUserUSD[1];
                }
                else {
                    $businessUserPrice = $businessUserPrice + $priceGroupBusinessUserUSD[1];
                }
            } else if ($i > 500) {
                $businessUserPrice = $businessUserPrice + $priceGroupBusinessUserUSD[2];
            }
        }
    }
    if ($currencyType !== "usd") {
        $totalPrice = round($totalPrice / 1.31, 2);
        $fullUserPrice = round($fullUserPrice / 1.31, 2);
        $businessUserPrice = round($businessUserPrice / 1.31, 2);
    }
    if (isset($_POST['premiumFeature'])) {
        $premiumUser = true;
        $totalPrice = $totalPrice + $premiumValue;
    }
    $totalPrice = number_format($totalPrice + $fullUserPrice + $businessUserPrice);
    try
    {
        //Get form data
        $formdata = array(
            'quotation_info' => array(
                'quotation_number' => $_POST['quotationNumber'],
                'quotation_date' => $_POST['quotationDate']
            ),
            'license_info' => array(
                'totalUsers' => $_POST['totalUsers'],
                'fullUser' => $_POST['fullUsers'],
                'businessUsers' => $_POST['businessUsers'],
                'premiumUser' => $premiumUser
            ),
            'pricing_info' => array(
                'total_price' => $totalPrice,
                'full_user_price' => number_format($fullUserPrice + $tempTotalPrice),
                'business_user_price' => number_format($businessUserPrice),
                'currency' => $_POST['radioCurrency'],
                'premiumPrice' => '60,000'
            )
        );
        $fp = fopen('assets/' . $_POST['quotationNumber'] . '.json', 'w');
        fwrite($fp, json_encode($formdata));
        fclose($fp);
        echo json_encode(['message' => 'Data saved successfully']);
    }
    catch (Exception $e) {
        echo 'Caught exception: ',  $e->getMessage(), "\n";
    }
}
catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}
