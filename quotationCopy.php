<?php
    $myFile = "assets/" . $_GET['quotation'] . ".json";
    $arr_data = array(); // create empty array
    $jsondata = file_get_contents($myFile);
    $arr_data = json_decode($jsondata, true);
    $currencySymbol = "&#36;";
    if ($arr_data['pricing_info']['currency'] === "usd") {
        $currencySymbol = "&#36;";
    } else {
        $currencySymbol = "&#163;";
    }
?>
<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <title>ContractPodAi - Price Calculator</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link rel="icon" href="favicon.ico">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="MDB/css/bootstrap.min.css">
    <link rel="stylesheet" href="MDB/css/mdb.min.css">
    <link rel="stylesheet" href="MDB/css/style.css">
    <link rel="stylesheet" href="css/iao-alert.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/quotation.css">
    <meta name="theme-color" content="#fafafa">
    <script src="js/modernizr-3.8.0.min.js"></script>
    <script type="text/javascript" src="MDB/js/jquery.min.js"></script>
    <script type="text/javascript" src="MDB/js/popper.min.js"></script>
    <script type="text/javascript" src="MDB/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="MDB/js/mdb.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="build/pdfmake.min.js"></script>
    <script src="build/vfs_fonts.js"></script>
    <script src="js/iao-alert.jquery.min.js"></script>
</head>

<body>
<!--[if IE]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->

<!-- Add your site or application content here -->
<div class="view_bg">
    <div class="container">
        <div class="row">
            <div class="col-md-11"></div>
            <div class="col-md-1">
                <div class="logout-container">
                    <a href="dataInputCopy.php">Regenerate</a>
                    <a href="index.php">Logout</a>
                </div>
            </div>
        </div>
        <?php
            if ($arr_data['license_info']['totalUsers'] > 1000 || $arr_data['license_info']['fullUser'] > 500) {
                ?>
                <div class="row pt-5">
                    <div class="col text-left" id="pageHeader">
                        <h2>This customer needs <span class="coloredText">a custom quote due to the volume of sale,</span>  please speak to your VP.</h2>
                    </div>
                </div>
                <?php
            }
            else {
                ?>
                <div id="invoice">
                    <div class="toolbar hidden-print">
                        <div class="text-right">
                            <button class="btn btn-custom-orange" onclick="generateReport()"><i class="fa fa-file-pdf-o"></i> Export as PDF
                            </button>
                        </div>
                        <hr>
                    </div>
                    <div class="invoice overflow-auto">
                        <div style="min-width: 600px">
                            <header>

                            </header>
                            <main>
                                <div class="row contacts">
                                    <div class="col invoice-to">
                                        <h4 class="invoice-subtitle">Customer Information</h4>
                                        <div class="address">Attn: <b><?php echo $arr_data['customer_info']['attention_to']; ?></b></div>
                                        <input type="hidden" name="attn" id="attn" value="<?php echo $arr_data['customer_info']['attention_to']; ?>">
                                        <div class="address"><?php echo $arr_data['customer_info']['address_line1']; ?>, <?php echo $arr_data['customer_info']['address_line2']; ?></div>
                                        <input type="hidden" name="addressLine1" id="addressLine1" value="<?php echo $arr_data['customer_info']['address_line1']; ?>">
                                        <input type="hidden" name="addressLine2" id="addressLine2" value="<?php echo $arr_data['customer_info']['address_line2']; ?>">
                                        <div class="address"><?php echo $arr_data['customer_info']['city_name']; ?>, <?php echo $arr_data['customer_info']['state_name']; ?></div>
                                        <input type="hidden" name="cityName" id="cityName" value="<?php echo $arr_data['customer_info']['city_name']; ?>">
                                        <input type="hidden" name="stateName" id="stateName" value="<?php echo $arr_data['customer_info']['state_name']; ?>">
                                        <div class="address"><?php echo $arr_data['customer_info']['postal_code']; ?>, <?php echo $arr_data['customer_info']['country_name']; ?></div>
                                        <input type="hidden" name="postalCode" id="postalCode" value="<?php echo $arr_data['customer_info']['postal_code']; ?>">
                                        <input type="hidden" name="countryName" id="countryName" value="<?php echo $arr_data['customer_info']['country_name']; ?>">
                                    </div>
                                    <div class="col invoice-details">
                                        <h4 class="invoice-subtitle">Quotation Information</h4>
                                        <div class="date"><?php echo $arr_data['quotation_info']['quotation_number']; ?></div>
                                        <input type="hidden" name="quotationNumber" id="quotationNumber" value="<?php echo $arr_data['quotation_info']['quotation_number']; ?>">
                                        <div class="date">Date of
                                            Quotation: <?php echo $arr_data['quotation_info']['quotation_date']; ?></div>
                                        <input type="hidden" name="quotationDate" id="quotationDate" value="<?php echo $arr_data['quotation_info']['quotation_date']; ?>">
                                        <div class="date">Expiry
                                            Date: <?php echo $arr_data['quotation_info']['expiry_date']; ?></div>
                                        <input type="hidden" name="expiryDate" id="expiryDate" value="<?php echo $arr_data['quotation_info']['expiry_date']; ?>">
                                    </div>
                                </div>
                                <table class="quote-table" border="0" cellspacing="0" cellpadding="0">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th class="text-left">DESCRIPTION</th>
                                        <th class="text-right">LICENSE COUNT</th>
                                        <th class="text-right">TOTAL</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="no">01</td>
                                        <td class="text-left">Total user licenses</td>
                                        <td class="unit"><?php echo $arr_data['license_info']['totalUsers']; ?></td>
                                        <td class="total"><?php echo $currencySymbol; ?><?php echo $arr_data['pricing_info']['total_price']; ?></td>
                                    </tr>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td colspan="3">SUBTOTAL</td>
                                        <td><?php echo $currencySymbol; ?><?php echo $arr_data['pricing_info']['total_price']; ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">TAX 0%</td>
                                        <td><?php echo $currencySymbol; ?> 0.00</td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">GRAND TOTAL</td>
                                        <td><?php echo $currencySymbol; ?><?php echo $arr_data['pricing_info']['total_price']; ?></td>
                                    </tr>
                                    </tfoot>
                                </table>
                                <input type="hidden" name="totalUser" id="totalUser" value="<?php echo $arr_data['license_info']['totalUsers']; ?>">
                                <input type="hidden" name="totalPrice" id="totalPrice" value="<?php echo $arr_data['pricing_info']['total_price']; ?>">
                                <input type="hidden" name="currencySymbol" id="currencySymbol" value="<?php echo $currencySymbol; ?>"><br><br>
                                <div class="notice">** Applicable tax will be recalculated at the time of order
                                    processing. **<br><br>
                                <div class="notices">
                                    <div><u><b>Note:</b></u></div>
                                        <small>
                                            In accordance with our terms of business and agreement with you, payment
                                            should be made in full <b>14</b> days from date of signature in favour of
                                            <b>ContractPod Technologies Limited</b><br>
                                            Please credit the above amount to our bank account details as
                                            follows:<br><br>
                                            <table class="footer">
                                                <tr>
                                                    <td><b>Account Name:</b> Western Union Business Solutions</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Bank Name:</b> The Bank of New York Mellon</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Bank Address:</b> 1 Wall Street, New York City, NY 10286, New
                                                        York
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><b>Account No:</b> 8900534389</td>
                                                </tr>
                                                <tr>
                                                    <td><b>ABA Number:</b> 021000018</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Swift Code:</b> IRVTUS3N</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Reference:</b> ContractPod Technologies Limited, NE1600
                                                        (please quote this reference number when submitting payment)
                                                    </td>
                                                </tr>
                                            </table>
                                            <br>
                                            <p><b>Please email your remittance advice to:</b> <u>accounts@contractpodai.com</u>
                                            </p>
                                        </small>
                                    </div>
                                </div>
                            </main>
                            <footer>
                                <b>© ContractPod Technologies Limited</b><br>
                                <span>Savoy Tower, Floor 9, 77 Renfrew Street, Glasgow, G2 3BZ, Scotland</span><br>
                                <span>Tel: +44(0).141.280.1600</span><br>
                                <span>ContractPod Technologies Limited is a company registered in Scotland: registered number 425646</span><br>
                                <b>VAT number: 150960419</b><br>
                            </footer>
                        </div>
                        <!--DO NOT DELETE THIS div. IT is responsible for showing footer always at the bottom-->
                        <div></div>
                    </div>
                </div>
                <?php
            }
        ?>
    </div>
</div>
<script src="js/generate.js"></script>
<script src="js/content.js"></script>
<script src="js/images.js"></script>
</body>
</html>
