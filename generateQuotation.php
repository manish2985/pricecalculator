<?php
$myFile = "assets/" . $_POST['quotationNumber'] . ".json";
$arr_data = array();
try
{
    $jsondata = file_get_contents($myFile);
    $arr_data = json_decode($jsondata, true);
    $arr_data['customer_info'] = array(
        'attention_to' => $_POST['attentionTo'],
        'address_line1' => $_POST['addressLine1'],
        'address_line2' => $_POST['addressLine2'],
        'city_name' => $_POST['cityName'],
        'state_name' => $_POST['stateName'],
        'postal_code' => $_POST['postalCode'],
        'country_name' => $_POST['countryName']
    );
    $jsondata = json_encode($arr_data, JSON_PRETTY_PRINT);
    if(file_put_contents($myFile, $jsondata)) {
        echo json_encode(['message' => 'Data saved successfully']);
    }
    else
        echo json_encode(['message' => 'Error saving data']);

}
catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}
