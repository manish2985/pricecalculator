<?php
  session_start();
  $myFile = "assets/data.json";
  $arr_data = array(); // create empty array

  try
  {
    //Get data from existing json file
    $jsondata = file_get_contents($myFile);

    // converts json data into array
    $arr_data = json_decode($jsondata, true);
    $found = current(array_filter($arr_data, function($item) {
      return ((isset($item['username']) && $_POST['username'] == $item['username']) && (isset($item['password']) && $_POST['password'] == $item['password']));
    }));

    if (!empty($found)) {
      $_SESSION["loggedIn"] = true;
      $_SESSION["name"] = $found['firstName'] . " " . $found['lastName'];
      echo "login successful";
    }
    else {
      echo "login failed";
    }
   }
   catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
   }
