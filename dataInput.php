<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <title>ContractPodAi - Price Calculator</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link rel="icon" href="favicon.ico">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="MDB/css/bootstrap.min.css">
    <link rel="stylesheet" href="MDB/css/mdb.min.css">
    <link rel="stylesheet" href="MDB/css/style.css">
    <link rel="stylesheet" href="css/iao-alert.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/jquery-ui.min.css">
    <meta name="theme-color" content="#fafafa">
    <script src="js/modernizr-3.8.0.min.js"></script>
    <script type="text/javascript" src="MDB/js/jquery.min.js"></script>
    <script type="text/javascript" src="MDB/js/popper.min.js"></script>
    <script type="text/javascript" src="MDB/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="MDB/js/mdb.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script src="js/iao-alert.jquery.min.js"></script>
</head>

<body>
<!--[if IE]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->

<!-- Add your site or application content here -->
<div class="view" style="background-image: url('img/background.png'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
    <div class="container">
        <div class="logout-container">
            <a href="index.php">Logout</a>
        </div>
        <div class="row d-flex justify-content-center">
            <div class="col-md-10 col-xl-6 py-5">
                <form class="text-center" style="color: #757575;" id="dataInputForm">
                    <!--Accordion wrapper-->
                    <div class="accordion md-accordion accordion-1" id="accordionEx23" role="tablist">
                        <div class="card">
                            <div class="card-header btn-custom-orange lighten-3 z-depth-1" role="tab" id="heading96">
                                <h5 class="text-uppercase mb-0 py-1">
                                    <a class="white-text font-weight-bold" data-toggle="collapse" href="#collapse96" aria-expanded="true"
                                       aria-controls="collapse96">
                                        Quotation Information <i class="fas fa-angle-down rotate-icon"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapse96" class="collapse show" role="tabpanel" aria-labelledby="heading96"
                                 data-parent="#accordionEx23">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="md-form mt-3">
                                                <label for="quotationNumber" class="active">* Quotation #</label>
                                                <input type="text" class="form-control is-valid" id="quotationNumber" name="quotationNumber" autocomplete="off">
                                                <input type="hidden">
                                            </div>
                                            <div class="md-form mt-3">
                                                <input type="text" id="quotationDate" name="quotationDate" class="form-control datepicker" required>
                                                <label for="quotationDate">* Quotation Date</label>
                                            </div>
                                            <div class="md-form mt-3">
                                                <input type="text" id="expiryDate" name="expiryDate" class="form-control datepicker" required>
                                                <label for="expiryDate">Expiry Date</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header btn-custom-orange lighten-3 z-depth-1" role="tab" id="heading97">
                                <h5 class="text-uppercase mb-0 py-1">
                                    <a class="collapsed font-weight-bold white-text" data-toggle="collapse" href="#collapse97"
                                       aria-expanded="false" aria-controls="collapse97">
                                        Customer Information
                                    </a>
                                </h5>
                            </div>
                            <div id="collapse97" class="collapse" role="tabpanel" aria-labelledby="heading97"
                                 data-parent="#accordionEx23">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="md-form mt-3">
                                                <label for="attentionTo">* Attention</label>
                                                <input type="text" class="form-control is-valid" id="attentionTo" name="attentionTo" autocomplete="off" required>
                                            </div>
                                            <div class="md-form mt-3">
                                                <label for="addressLine1">* Street address, P.O. box, company name, c/o</label>
                                                <input type="text" class="form-control is-valid" id="addressLine1" name="addressLine1" autocomplete="off" required>
                                            </div>
                                            <div class="md-form mt-3">
                                                <label for="addressLine2">Apartment, suite , unit, building, floor, etc.</label>
                                                <input type="text" class="form-control is-valid" id="addressLine2" name="addressLine2" autocomplete="off">
                                            </div>
                                            <div class="form-row">
                                                <div class="col">
                                                    <div class="md-form mt-3">
                                                        <label for="cityName">* City / Town</label>
                                                        <input type="text" class="form-control is-valid" id="cityName" name="cityName" autocomplete="off" required>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="md-form mt-3">
                                                        <label for="stateName">* State / Province / Region</label>
                                                        <input type="text" class="form-control is-valid" id="stateName" name="stateName" autocomplete="off" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col">
                                                    <div class="md-form mt-3">
                                                        <label for="postalCode">* Zip / Postal Code</label>
                                                        <input type="text" class="form-control is-valid" id="postalCode" name="postalCode" autocomplete="off" required>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="md-form mt-3">
                                                        <label for="countryName">* Country</label>
                                                        <input type="text" class="form-control is-valid" id="countryName" name="countryName" autocomplete="off" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header btn-custom-orange lighten-3 z-depth-1" role="tab" id="heading98">
                                <h5 class="text-uppercase mb-0 py-1">
                                    <a class="collapsed font-weight-bold white-text" data-toggle="collapse" href="#collapse98"
                                       aria-expanded="false" aria-controls="collapse98">
                                        License Count
                                    </a>
                                </h5>
                            </div>
                            <div id="collapse98" class="collapse" role="tabpanel" aria-labelledby="heading98"
                                 data-parent="#accordionEx23">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <!-- Total users -->
                                            <div class="md-form mt-3">
                                                <label for="totalUsers">Total number of users</label>
                                                <input type="number" class="form-control is-valid" id="totalUsers" name="totalUsers" autocomplete="off" required>
                                                <div class="invalid-feedback">
                                                    Please enter a valid number of total users.
                                                </div>
                                            </div>
                                            <div class="md-form mt-3">
                                                <label for="fullUsers">Total number of Full users</label>
                                                <input type="number" class="form-control is-valid" id="fullUsers" name="fullUsers" autocomplete="off">
                                            </div>
                                            <div class="md-form mt-3">
                                                <label for="businessUsers">Total number of Business users</label>
                                                <input type="number" class="form-control is-valid" id="businessUsers" name="businessUsers" autocomplete="off">
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input type="radio" class="form-check-input" id="radioUSD" name="radioCurrency" value="usd" checked>
                                                <label class="form-check-label" for="radioUSD">USD</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input type="radio" class="form-check-input" id="radioGBP" name="radioCurrency" value="gbp">
                                                <label class="form-check-label" for="radioGBP">GBP</label>
                                            </div>
                                            <!-- Sign in button -->
                                            <button class="btn btn-custom-orange btn-rounded btn-block z-depth-0 my-4 waves-effect" type="submit" id="btnCalculate">Generate Quotation</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Accordion wrapper-->
                </form>
            </div>
        </div>
    </div>
</div>
<script src="js/scripts.js"></script>
<script>
    $.ajax({
        dataType: 'json',
        type: "GET",
        url: 'utils.php',
        success: function(response){
            $('#quotationNumber').val(response.data);
        }
    });
    $('.datepicker').datepicker();
    $('#dataInputForm').submit (function (e) {
        e.preventDefault();
        $.ajax({
            url: 'calculate.php',
            dataType: 'json',
            type: 'post',
            contentType: 'application/x-www-form-urlencoded',
            data: $('#dataInputForm').serialize(),
            success: function( data, textStatus, jQxhr ){
                if (data) {
                    window.location = "quotation.php?quotation=" + $('#quotationNumber').val();
                }
                else {
                    $.iaoAlert({
                        msg: "Some error occurred while calculating final price. Please contact admin.",
                        type: "error",
                        mode: "dark",
                        autoHide: true,
                        alertTime: "6000",
                        position: 'top-right',
                        fadeOnHover: false,
                        zIndex: '999'
                    });
                }
            },
            error: function( jqXhr, textStatus, errorThrown ){
                $.iaoAlert({
                    msg: "Some error occurred while calculating final price. Please contact admin.",
                    type: "error",
                    mode: "dark",
                    autoHide: true,
                    alertTime: "6000",
                    position: 'top-right',
                    fadeOnHover: false,
                    zIndex: '999'
                });
            }
        });
    });
</script>
</body>

</html>
