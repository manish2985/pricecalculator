<?php
    session_start();
?>
<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <title>ContractPodAi - Price Calculator</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link rel="icon" href="favicon.ico">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="MDB/css/bootstrap.min.css">
    <link rel="stylesheet" href="MDB/css/mdb.min.css">
    <link rel="stylesheet" href="MDB/css/style.css">
    <link rel="stylesheet" href="css/iao-alert.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/jquery-ui.min.css">
    <meta name="theme-color" content="#fafafa">
    <script src="js/modernizr-3.8.0.min.js"></script>
    <script type="text/javascript" src="MDB/js/jquery.min.js"></script>
    <script type="text/javascript" src="MDB/js/popper.min.js"></script>
    <script type="text/javascript" src="MDB/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="MDB/js/mdb.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/moment.min.js"></script>
    <script src="js/iao-alert.jquery.min.js"></script>
</head>

<body>
<!--[if IE]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->

<!-- Add your site or application content here -->
<div class="view_bg">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="title-container">
                    <h3><?php echo $_SESSION["name"]; ?></h3>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-6">
                        <div class="title-container">
                            <a href="index.php">Logout</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row d-flex justify-content-center">
            <div class="col-md-10 col-xl-6 py-5">
                <div class="card">
                    <div class="card-header card-image d-flex justify-content-center">
                        <h3>Pricing Calculation Input</h3>
                    </div>
                    <div class="card-body px-lg-5">
                        <form class="text-center" style="color: #757575;" id="dataInputForm">
                            <div class="md-form mt-3">
                                <label for="quotationNumber" class="active">* Quotation #</label>
                                <input type="text" class="form-control is-valid" id="quotationNumber" name="quotationNumber" autocomplete="off">
                            </div>
                            <div class="md-form mt-3">
                                <input type="text" id="quotationDate" name="quotationDate" class="form-control datepicker" required>
                                <label for="quotationDate">* Quotation Date</label>
                            </div>
<!--                            <div class="md-form mt-3">-->
<!--                                <input type="text" id="expiryDate" name="expiryDate" class="form-control datepicker"  autocomplete="off">-->
<!--                                <label for="expiryDate">Expiry Date</label>-->
<!--                            </div>-->
                            <div class="md-form mt-3">
                                <label for="fullUsers">Total number of Full users</label>
                                <input type="number" class="form-control is-valid" id="fullUsers" name="fullUsers" autocomplete="off">
                            </div>
                            <div class="md-form mt-3">
                                <label for="businessUsers">Total number of Business users</label>
                                <input type="number" class="form-control is-valid" id="businessUsers" name="businessUsers" autocomplete="off">
                            </div>
                            <div class="md-form mt-3">
                                <label for="totalUsers">Total number of users</label>
                                <input type="number" class="form-control is-valid" id="totalUsers" name="totalUsers" autocomplete="off">
                            </div>
                            <div class="md-form mt-3">
                                <div class="form-check form-check-inline">
                                    <input type="radio" class="form-check-input" id="radioUSD" name="radioCurrency" value="usd" checked>
                                    <label class="form-check-label" for="radioUSD">USD</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input type="radio" class="form-check-input" id="radioGBP" name="radioCurrency" value="gbp">
                                    <label class="form-check-label" for="radioGBP">GBP</label>
                                </div>
                            </div>
                            <div class="md-form mt-3">
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="premiumFeature" name="premiumFeature" value="1">
                                    <label class="form-check-label" for="premiumFeature">Customer wants Contract Risk & Compliance feature</label>
                                </div>
                            </div>
                            <!-- Sign in button -->
                            <button class="btn btn-custom-orange btn-rounded btn-block z-depth-0 my-4 waves-effect" type="submit" id="btnCalculate">Calculate</button>
                        </form>
                        <!-- Form -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="js/scripts.js"></script>
<script>
    $.ajax({
        dataType: 'json',
        type: "GET",
        url: 'utils.php',
        success: function(response){
            $('#quotationNumber').val(response.data);
            $('#quotationDate').val(moment().format('D MMM, YYYY')).parent().find('label').addClass('active');
        }
    });
    $('.datepicker').datepicker({ dateFormat: 'd M, y' });
    $('#fullUsers').on('change', function () {
        if ($('#businessUsers').val() !== "") {
            var totalUser = parseInt($(this).val()) + parseInt($('#businessUsers').val());
            $('#totalUsers').val(totalUser).parent().find('label').addClass('active');
        }
        else {
            var totalUser = parseInt($(this).val());
            $('#totalUsers').val(totalUser).parent().find('label').addClass('active');
        }
    });
    $('#businessUsers').on('change', function () {
        if ($('#fullUsers').val() !== "") {
            var totalUser = parseInt($(this).val()) + parseInt($('#fullUsers').val());
            $('#totalUsers').val(totalUser).parent().find('label').addClass('active');
        }
        else {
            var totalUser = parseInt($(this).val());
            $('#totalUsers').val(totalUser).parent().find('label').addClass('active');
        }
    });
    $('#dataInputForm').submit (function (e) {
        e.preventDefault();
        $.ajax({
            url: 'calculateCopy.php',
            dataType: 'json',
            type: 'post',
            contentType: 'application/x-www-form-urlencoded',
            data: $('#dataInputForm').serialize(),
            success: function( data, textStatus, jQxhr ){
                if (data) {
                    window.location = "calculationResult.php?quotation=" + $('#quotationNumber').val();
                }
                else {
                    $.iaoAlert({
                        msg: "Some error occurred while calculating final price. Please contact admin.",
                        type: "error",
                        mode: "dark",
                        autoHide: true,
                        alertTime: "6000",
                        position: 'top-right',
                        fadeOnHover: false,
                        zIndex: '999'
                    });
                }
            },
            error: function( jqXhr, textStatus, errorThrown ){
                $.iaoAlert({
                    msg: "Some error occurred while calculating final price. Please contact admin.",
                    type: "error",
                    mode: "dark",
                    autoHide: true,
                    alertTime: "6000",
                    position: 'top-right',
                    fadeOnHover: false,
                    zIndex: '999'
                });
            }
        });
    });
</script>
</body>

</html>
