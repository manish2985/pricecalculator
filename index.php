<?php
session_start();
unset($_SESSION["loggedIn"]);
unset($_SESSION["firstName"]);
?>
<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title>ContractPodAi - Price Calculator</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="manifest" href="site.webmanifest">
  <link rel="apple-touch-icon" href="icon.png">
  <!-- Place favicon.ico in the root directory -->
  <link rel="icon" href="favicon.ico">
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="MDB/css/bootstrap.min.css">
  <link rel="stylesheet" href="MDB/css/mdb.min.css">
  <link rel="stylesheet" href="MDB/css/style.css">
  <link rel="stylesheet" href="css/iao-alert.min.css">
  <link rel="stylesheet" href="css/styles.css">
  <meta name="theme-color" content="#fafafa">
    <script src="js/modernizr-3.8.0.min.js"></script>
  <script type="text/javascript" src="MDB/js/jquery.min.js"></script>
  <script type="text/javascript" src="MDB/js/popper.min.js"></script>
  <script type="text/javascript" src="MDB/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="MDB/js/mdb.min.js"></script>
  <script src="js/iao-alert.jquery.min.js"></script>
</head>

<body>
<!--[if IE]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->

<!-- Add your site or application content here -->
<!-- Full Page Intro -->
<div class="view" style="background-image: url('img/cover.png'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
  <!-- Mask & flexbox options-->
  <div class="mask rgba-gradient d-flex justify-content-center align-items-center">
    <!-- Content -->
    <div class="container">
      <!--Grid row-->
      <div class="row">
        <div class="col-6"></div>
        <div class="col-6">
          <!-- Material form subscription -->
          <div class="card">
            <div class="card-header card-image d-flex justify-content-center">
              <img src="img/logo/logo-black-x1.png" />
            </div>
            <!--Card content-->
            <div class="card-body px-lg-5">
              <!-- Form -->
              <form class="text-center" style="color: #757575;" id="loginForm">
                <!-- Name -->
                <div class="md-form mt-3">
                  <label for="username">Username</label>
                  <input type="text" class="form-control is-valid" id="username" name="username" autocomplete="off" required>
                  <div class="invalid-feedback">
                    Please enter a valid username.
                  </div>
                </div>
                <div class="md-form mt-3">
                  <label for="password">Password</label>
                  <input type="password" class="form-control is-valid" id="password" name="password" autocomplete="off" required>
                  <div class="invalid-feedback">
                    Please enter a valid password.
                  </div>
                </div>
                <!-- Sign in button -->
                <button class="btn btn-custom-orange btn-rounded btn-block z-depth-0 my-4 waves-effect" type="submit" id="loginBtn">Login</button>
              </form>
              <!-- Form -->
            </div>
          </div>
          <!-- Material form subscription -->
        </div>
      </div>
      <!--Grid row-->
    </div>
    <!-- Content -->
  </div>
  <!-- Mask & flexbox options-->
</div>
<!-- Full Page Intro -->

<script src="js/scripts.js"></script>
<script>
    $('#loginForm').submit (function (e) {
        e.preventDefault();
        $.ajax({
            url: 'login.php',
            dataType: 'text',
            type: 'post',
            contentType: 'application/x-www-form-urlencoded',
            data: $(this).serialize(),
            success: function( data, textStatus, jQxhr ){
                if (data === "login successful") {
                    window.location = "dataInputCopy.php";
                }
                else {
                    $('#password').val("");
                    $.iaoAlert({
                        msg: "Login failed! Please check the credentials.",
                        type: "error",
                        mode: "dark",
                        autoHide: true,
                        alertTime: "6000",
                        position: 'top-right',
                        fadeOnHover: false,
                        zIndex: '999'
                    });
                }
            },
            error: function( jqXhr, textStatus, errorThrown ){
                $('#password').val("");
                $.iaoAlert({
                    msg: "Login failed! Please check the credentials.",
                    type: "error",
                    mode: "dark",
                    autoHide: true,
                    alertTime: "6000",
                    position: 'top-right',
                    fadeOnHover: false,
                    zIndex: '999'
                });
            }
        });
    });
</script>
</body>

</html>
